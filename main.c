#include "defer.h"

#include <stdio.h>

int main()
{
  int result = 0;
  defer_block {
    FILE* log_file = fopen("log.txt", "r");
    if (!log_file) {
      result = -1; defer_return;
    }
    defer(fclose(log_file));

    defer_scope {
      FILE* info_log_file = fopen("info_log.txt", "w");
      if (!info_log_file) {
        result = -1; defer_return;
      }
      defer(fclose(info_log_file));

      FILE* warning_log_file = fopen("warning_log.txt", "w");
      if (!warning_log_file) {
        result = -1; defer_return;
      }
      defer(fclose(warning_log_file));

    } run_deferred();

    FILE* error_log_file = fopen("error_log.txt", "w");
    if (!error_log_file) {
      result = -1; defer_return;
    }
    defer(fclose(error_log_file));
  } run_deferred();
  return result;
}
