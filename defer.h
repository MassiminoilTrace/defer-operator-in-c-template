#define defer_break { goto _d_start; }
#define defer_return { _d_r = 1; goto _d_start; }
#define defer(F) {int _d_p = _d; \
  _d = __LINE__; if(0){case __LINE__: _d = _d_p; F; goto _d_start;}}
#define defer_block int _d_r = 0; int _d = 0; \
  _d_start: switch(_d){case 0: _d = -1;
#define run_deferred() goto _d_start; } while (0)

#define _DEFER_SNAPSHOT(A) {int _d_p = _d; \
  _d = __LINE__; if(0){case __LINE__: _d = _d_p; A;}}
#define defer_scope \
  do { _DEFER_SNAPSHOT(if(_d_r){goto _d_start;} else {continue;})
